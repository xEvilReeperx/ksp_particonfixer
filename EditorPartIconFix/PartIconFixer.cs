﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using UnityEngine;
using ReeperCommon;

#if DEBUG
namespace PartIconFixer_DEBUG
{
#endif

// Note: Run this version in debug build because a lot of intellisense and autocompletion
// features are disabled in the tt, which makes it a hassle. So development will be done in
// the DEBUG namespace, and then on release run the tt before compiling to get an updated,
// assembly-version-dependent namespace that the following code is essentially copy+pasted into
#if (DEBUG && !VERSION_NAMESPACE_CREATED) || VERSION_NAMESPACE_CREATED
    [KSPAddon(KSPAddon.Startup.Instantly, true)]
    internal class PartIconFixer : LoadingSystem
    {
        private const float IconCamHalfSize = 40f;
        private const string MultiplierValueName = "iconMultiplier";
        private const string PivotValueName = "iconPivot";
        private const string RotationValueName = "iconRotation";
        
        private static bool _ran = false;

        // Keep track of a stack of AvailableParts and their associated ConfigNodes (so we can look up
        // the multiplier/pivot values, if present). The Run routine will work through them one at a time
        private readonly Stack<KeyValuePair<AvailablePart, ConfigNode>> _targets =
            new Stack<KeyValuePair<AvailablePart, ConfigNode>>();

        private float _startTime = 0f;
        private int _targetCount = 0;



        /// <summary>
        /// Get ourselves inserted into the loader
        /// </summary>
        /// <returns></returns>
        [Annotations.UsedImplicitly]
        private System.Collections.IEnumerator Start()
        {
            if (_ran)
            {
                Log.Debug("Instance already running, terminating {0}", string.Format("{0} v{1}", Assembly.GetExecutingAssembly().GetName().Name, Assembly.GetExecutingAssembly().GetName().Version));
                yield break;
            }

            _ran = true; // don't want any extra copies of this particular type inserting themselves

            var currentVersion = Assembly.GetExecutingAssembly().GetName().Version;

            // now, are we responsible for logic?
            if (AssemblyLoader.loadedAssemblies.Where(
                la => la.assembly.GetName().Name == Assembly.GetExecutingAssembly().GetName().Name)
                .Select(la => la.assembly.GetName().Version)
                .OrderByDescending(a => a)
                .First().CompareTo(currentVersion) == 0)
            {
                Log.Normal("{0} will run.", currentVersion);
            }
            else
            {
                Log.Normal("{0} disabled because it is an older version.", currentVersion);
                yield break;
            }




            Log.Normal("{0} starting", string.Format("{0} v{1}", Assembly.GetExecutingAssembly().GetName().Name, Assembly.GetExecutingAssembly().GetName().Version));

            yield return new WaitForEndOfFrame(); // we absolutely want to be last in the loading order

            var screen = FindObjectOfType<LoadingScreen>();
            if (screen == null)
            {
                Log.Error("Failed to find LoadingScreen");
            }
            else
            {
                screen.loaders.Add(this);
                
#if DEBUG
                screen.loaders.ForEach(ls => Log.Normal("LoadingSystem: {0}", ls.GetType().FullName));
#endif
            }
        }



        /// <summary>
        /// Returns true when we're done patching icons
        /// </summary>
        /// <returns></returns>
        public override bool IsReady()
        {
            return _targets.Count == 0;
        }



        /// <summary>
        /// Added only because having an uncommented method bothers me
        /// </summary>
        /// <returns></returns>
        public override float ProgressFraction()
        {
            return _targetCount > 0 ? 1f - _targets.Count/(float) _targetCount : 1f;
        }


        /// <summary>
        /// Let the user know what we're working on
        /// </summary>
        /// <returns></returns>
        public override string ProgressTitle()
        {
            return _targets.Count > 0
                ? string.Format("Editor Part Icon: {0}", _targets.Peek().Key.name)
                : "Editor Part Icons: Finished";
        }



        /// <summary>
        /// KSP calls this when it's our turn to start doing our thing. Gather up a list of part icons that
        /// need tweaking and fire up Run()
        /// </summary>
        public override void StartLoad()
        {
            base.StartLoad();

            _startTime = Time.realtimeSinceStartup;
            var urlCfgs = GameDatabase.Instance.root.GetConfigs("PART");
            bool missingConfigs = false;

            PartLoader.LoadedPartsList.ForEach(ap =>
            {
                try
                {
                    Log.Debug("Checking '{0}' for skinned mesh renderers or PartIconFixer keywords...", ap.name);

                    // note to self:
                    //
                    // underscore in PART config => replaced with period
                    // period in PART config => period in part name

                    // find the urlCfg which matches this part (ap.name apparently isn't sanitized and so matches
                    // the name in part config exactly)
                    var matches = urlCfgs.Where(url => url.name == KSPUtil.SanitizeFilename(ap.name));

                        // there are apparently cases where ap.name shouldn't be sanitized (??)
                        if (matches.Count() == 0) matches = urlCfgs.Where(url => url.name == ap.name);


                    if (matches.Count() > 1)
                    {
                        Log.Error("There appear to be multiple [{1}] instances of '{0}'; can't match their names to appropriate configs so they will be skipped.", ap.name,
                            matches.Count());
                        Log.Normal("Matches are:");
                        matches.ToList().ForEach(match => Log.Normal("{0} at {1}", match.name, match.url));
                        return;
                    }

                    var urlCfg = matches.FirstOrDefault();

                    if (urlCfg != null)
                    {
                        // if this part has special parameters for us OR it's skinned (can be both of course), then it's a target
                        if (urlCfg.config.HasValue(MultiplierValueName) ||
                            urlCfg.config.HasValue(PivotValueName) ||
                            urlCfg.config.HasValue(RotationValueName) ||
                            ap.partPrefab.gameObject.GetComponentsInChildren<SkinnedMeshRenderer>(true).Length > 0)
                        {
                            _targets.Push(new KeyValuePair<AvailablePart, ConfigNode>(ap, urlCfg.config));
                            Log.Debug("Added target {0}", ap.name);
                        }
                    }
                    else if (ap.category != PartCategories.none)
                        // not having a found config only concerns us if the part will appear in the editor
                    {
                        Log.Warning(
                            "Didn't find a ConfigNode for {0}", ap.name);

                        missingConfigs = true;
                    }
                }
                catch (Exception e)
                {
                    Log.Error("Encountered an exception while processing '{0}' - {1}", ap.name, e);
                }
            });

            _targetCount = _targets.Count;

            Log.Normal("Ready to process {0} items:", _targetCount);

            if (missingConfigs)
            {
#if DEBUG
                Log.Debug("Listing all Config names");
                urlCfgs.ToList().ForEach(cfg => Log.Normal("cfg: {0}", cfg.name));
#endif
            }

            StartCoroutine(Run());
        }


        /// <summary>
        /// Works through the list of parts that need their icons fixed, one per frame
        /// </summary>
        /// <returns></returns>
        private System.Collections.IEnumerator Run()
        {
            while (_targets.Count > 0)
            {
                yield return 0;

                try
                {
                    var next = _targets.Peek();

                    // grab supplied special values (or use defaults)
                    float multiplier = next.Value.Parse(MultiplierValueName, 1f);
                    string pivot = next.Value.Parse(PivotValueName);
                    Vector3 rotation = Vector3.zero;
                    
                    if (next.Value.HasValue(RotationValueName))
                        rotation = KSPUtil.ParseVector3(next.Value.Parse(RotationValueName, "0, 0, 0"));

                    Log.Normal("Fixing {0} part icon with multiplier = {1}, pivot = {2}, rotation = {3}", next.Key.name, multiplier,
                        string.IsNullOrEmpty(pivot) ? "\"\"" : pivot, rotation);


                    FixIcon(next.Key, multiplier, pivot, rotation);

                    _targets.Pop();
                }
                catch (Exception e)
                {
                    Log.Error("Exception in EditorIconScaleFix.Run: {0}", e);
                    Log.Error("part = " + _targets.Peek().Key.name);
                    Log.Error("multiplier = " + _targets.Peek().Value.GetValue(MultiplierValueName));
                    Log.Error("pivot = " + _targets.Peek().Value.GetValue(PivotValueName));
                    Log.Error("rotation = {0} (default {1})", _targets.Peek().Value.GetValue(RotationValueName), Vector3.zero.ToString());

                    _targets.Clear(); // something's hosed ;\
                    yield break;
                }
            }

            Log.Normal("Finished in {0} seconds", (Time.realtimeSinceStartup - _startTime).ToString("F2"));

            yield return 0; // one more pause to update the progress bar text
        }



        /// <summary>
        /// Rescales the iconPrefab of given part.
        /// </summary>
        /// <param name="part">Part that needs scaling</param>
        /// <param name="multiplier">Allows part creator to fine-tune the final size of the icon</param>
        /// <param name="pivot">The icon carousel effect will rotate around this transform, or center of mass if left empty</param>
        /// <param name="rotation">A rotation offset to be applied to the icon</param>
        private void FixIcon(AvailablePart part, float multiplier, string pivot, Vector3 rotation)
        {
            GameObject iconPrefab = part.iconPrefab;


            // figure out what the largest dimension is
            //Bounds bounds = CalculateBounds(part.partPrefab.gameObject);
            Bounds bounds = CalculateBounds(iconPrefab);

            float max = Mathf.Max(bounds.size.x, bounds.size.y, bounds.size.z);

            // we want that max dimension to be half of the editor icon's desired size (it seems to always be set at
            // 80 units), so if we put this object in front of the camera its maximum dimension will be 80 units
            // (times any modder-supplied multiplier)
            float factor = IconCamHalfSize / max * multiplier;

            // finally, the editor seems to set the cloned prefab's root transform to localScale (40,40,40)
            // so we need to scale to 1/40th size
            factor /= 40f;

            iconPrefab.transform.GetChild(0).localScale *= factor; // use this if bounds based on iconPrefab
            //iconPrefab.transform.GetChild(0).localScale = Vector3.one*factor; // use this if bounds based on partPrefab

            // note: this appears to modify the "scale" indicator in the tooltip window and
            // doesn't contribute to actual final size in any way
            part.iconScale = 1f / max;


            // apply rotation before any pivot changes, else we'll end up rotating around the
            // wrong location
            iconPrefab.transform.GetChild(0).Rotate(rotation, Space.Self);


            // some parts will be off-center because their center of mass is based on colliders in the hierarchy
            // of the part; this will sometimes give incorrect results
            if (!string.IsNullOrEmpty(pivot))
            {

                Transform model = iconPrefab.transform.GetChild(0).Find("model");
                if (model == null)
                {
                    Log.Debug("{0} does not have a model transform", part.name);
                    // not necessarily a problem (kerbalEva doesn't have one for instance), but
                    // unusual
                    model = iconPrefab.transform.GetChild(0);
                }


                Transform target = model.Find(pivot);

                if (target == null)
                {
                    Log.Warning("Did not find pivot '{0}' for part {1}", pivot, part.name);
#if DEBUG
    // I thought it might be handy to list all valid pivot names in case we didn't
    // find the target due to a misspelling or something like that (this only happens in debug
    // builds so only the devs will see this output)
                    Log.Debug("Possible pivot names:");

                    // Map path from [Transform] to ["model"], with model being the above (either "model" or first child of iconPrefab)
                    Func<Transform, string> getPath = null;
                    Action<Transform> traverse = null;


                    // return a string containing the transform path from [model] to the given child transform
                    getPath = (Transform t) => t.parent != model && t != model ? getPath(t.parent) + "/" + t.name : t.name;

                    // Recursively iterate over a transform and all descendants
                    traverse = (Transform t) =>
                        {
                            if (t != model)
                                Log.Normal(getPath(t));
                            foreach (Transform child in t) traverse(child);
                        };


                    traverse(model);
    #endif
                }
                else
                {
                    iconPrefab.transform.GetChild(0).position -= target.position;
                    // remember we don't want to mess with the top level
                    // GO; it seems to be re-positioned on the fly in some
                    // relationship to bounds or iconScale
                    Log.Debug("Centering icon on pivot {0}", pivot);
                }
            }
            else // no pivot specified
            {
                iconPrefab.transform.GetChild(0).localPosition = Vector3.zero;
            }


        }



        /// <summary>
        /// It basically does what it says. Only accounts for MeshRenderers and SkinnedMeshRenderers. Bounds
        /// are approximated using worldspace AABBs
        /// </summary>
        /// <param name="go"></param>
        /// <returns></returns>
        private static Bounds CalculateBounds(GameObject go)
        {
            var renderers = go.GetComponentsInChildren<Renderer>(true).ToList();

            if (renderers.Count == 0) return default(Bounds);

            var boundsList = new List<Bounds>();

            renderers.ForEach(r =>
            {
                if (!r.enabled && (r is SkinnedMeshRenderer || r is MeshRenderer))
                    Log.Warning("{0} is not enabled", r.name);
                        // why wouldn't it be enabled? not necessarily a problem though

                if (r is SkinnedMeshRenderer)
                {
                    var smr = r as SkinnedMeshRenderer;

                    // the localBounds of the SkinnedMeshRenderer are initially large enough
                    // to accomodate all animation frames; they're likely to be far off for 
                    // parts that do a lot of animation-related movement (like solar panels expanding)
                    //
                    // We can get correct mesh bounds by baking the current animation into a mesh
                    // note: vertex positions in baked mesh are relative to smr.transform; any scaling
                    // is already baked in
                    Mesh mesh = new Mesh();
                    smr.BakeMesh(mesh);

                    // while the mesh bounds will now be correct, they don't consider orientation at all.
                    // If a long part is oriented along the wrong axis in world space, the bounds we'd get
                    // here could be very wrong. We need to come up with essentially the renderer bounds:
                    // a bounding box in world space that encompasses the mesh
                    Matrix4x4 m = Matrix4x4.TRS(smr.transform.position, smr.transform.rotation, Vector3.one
                        /* remember scale already factored in!*/);
                    var vertices = mesh.vertices;

                    Bounds smrBounds = new Bounds(m.MultiplyPoint3x4(vertices[0]), Vector3.zero);

                    for (int i = 1; i < vertices.Length; ++i)
                        smrBounds.Encapsulate(m.MultiplyPoint3x4(vertices[i]));

                    Destroy(mesh);

                    boundsList.Add(smrBounds);
                }
                else if (r is MeshRenderer) // note: there are ParticleRenderers, LineRenderers, and TrailRenderers
                {
                    r.gameObject.GetComponent<MeshFilter>().sharedMesh.RecalculateBounds();
                    boundsList.Add(r.bounds);
                }
            });


            Bounds bounds = boundsList[0];
            boundsList.Skip(1).ToList().ForEach(b => bounds.Encapsulate(b));

            return bounds;
        }
    }
#endif

#if DEBUG
}
#endif